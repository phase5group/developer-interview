This project is a sandbox for candidates interviewing for an engineering position to provide their demo submissions.

The following are eligible demo concepts.  The candidate should pick one and follow the instructions.


Task App
--------

Create a Django project with an task-tracking application. The goal of the application is to store/manage task items. A user should be able to create, change, and delete tasks.  Tasks can be assigned to any user.  Tasks should have at least a title and a description, and they should be capable of being displayed as a group in some fashion relative to the current month.


Quiz App
--------

Create a Django project with a quiz application. The goal of the application is to manage three things:

1. Instructional content that forms the basis of the quiz
1. Questions about that content (the quiz itself)
1. Answers (a given user’s interaction with the quiz)

An admin user should be able to create instructional content and questions from the admin interface, and a user should be able to view each piece of instructional content along with the questions, to which they can provide answers. The answers are boolean (i.e. yes/no). A user should be able to view all distinct quizzes, and be able to answer the questions to provide their submissions.


Instructions
============

1. Pick one of the mini-projects listed above and design and implement it.
1. Provide your solution to us as one of the following:
  * A pull request to this project
  * A gitlab-hosted fork of this project
  * A standalone git repo hosted elsewhere
1. Anything not explicitly mentioned in the description is left for you to interpret as you wish. It’s to your advantage to describe your interpretations.
1. Include documentation as you see fit.
1. Include tests as you see fit.
1. We should be able to easily and reliably setup and run your solution in a local development environment.
1. Please resist the urge to just assemble 2nd or 3rd party packages as the bulk of your solution, or to layer on dependencies unnecessarily. The purpose of this exercise is for you to demonstrate effective use of the designated platform (Django) via direct application of its native capabilities, and to do so with your own personal design and implementation effort. We’d rather see a more modestly-scoped solution that consists entirely of your own code than something more ambitious that sits on top of a bunch of extra stuff collected off the internet.
1. Don’t spend an inordinate amount of time on this. Our expectations are calibrated for a reasonable effort having been invested for the sake of an interview. We know you have a life and things competing for your time and energy, and we’re not expecting you to treat this exercise as though you were cramming for a semester project in college. How much time you devote to it is up to you, and you’re encouraged to tell us how much that was. If you knock something out in a couple hours and feel comfortable submitting it with a note that says “Hey, I spent a couple hours on this”, that’s fine and we’ll evaluate it objectively with that caveat in mind.

Good luck and thanks in advance for your effort and interest.
